#include <avr/sleep.h>
#include <EEPROM.h>
#include <Wire.h>
#include <OzOLED.h>
#include <AmperkaKB.h>
#include <iarduino_RTC.h>


AmperkaKB KB(4, 8, 7, 6, 5);
iarduino_RTC watch(RTC_DS1302, 11, 9, 10);

//  Определяем системное время:                           // Время загрузки скетча.
const char* strM="JanFebMarAprMayJunJulAugSepOctNovDec";  // Определяем массив всех вариантов текстового представления текущего месяца.
const char* sysT=__TIME__;                                // Получаем время компиляции скетча в формате "SS:MM:HH".
const char* sysD=__DATE__;                                // Получаем дату  компиляции скетча в формате "MMM:DD:YYYY", где МММ - текстовое представление текущего месяца, например: Jul.
//  Парсим полученные значения sysT и sysD в массив i:    // Определяем массив «i» из 6 элементов типа int, содержащий следующие значения: секунды, минуты, часы, день, месяц и год компиляции скетча.
const int i[6] {(sysT[6]-48)*10+(sysT[7]-48), (sysT[3]-48)*10+(sysT[4]-48), (sysT[0]-48)*10+(sysT[1]-48), (sysD[4]-48)*10+(sysD[5]-48), ((int)memmem(strM,36,sysD,3)+3-(int)&strM[0])/3, (sysD[9]-48)*10+(sysD[10]-48)};

const unsigned long crc_table[16] = {         // Таблица заранее просчитанных констант для ускорения расчета CRC
  0x00000000, 0x1db71064, 0x3b6e20c8, 0x26d930ac,
  0x76dc4190, 0x6b6b51f4, 0x4db26158, 0x5005713c,
  0xedb88320, 0xf00f9344, 0xd6d6a3e8, 0xcb61b38c,
  0x9b64c2b0, 0x86d3d2d4, 0xa00ae278, 0xbdbdf21c
};

const unsigned long freeEEPROM = 4288708479;  // Значение CRC неинициализированной памяти

int addressOffTime = 0;

const int outputPin = 3;
uint8_t onTime_h = 10; /* Включаем полив в 10:00:00 */
uint8_t onTime_m = 0;
uint8_t onTime_s = 0;
uint8_t offTime_h = 10; /* Выключаем полив в 10:00:05 */
uint8_t offTime_m = 0;
uint8_t offTime_s = 5;
String onTimeString, offTimeString; /* Переменные для отображения времени начала и окончания полива на экране */

// Изначально SIG на IRF520 выключен
int sig = LOW;             // SIG used to set the outputPin

const int btnStartPin = 2;     // the number of the pushbutton pin
const int ledPin =  13;      // the number of the LED pin

int btnStartState = LOW;         // variable for reading the pushbutton status

unsigned long currentMillis = 0;

// Таймер дисплея
const unsigned long onTimeDisplay = 10000UL; /* Время активности дисплея 10 секунд */
unsigned long previousMillisDisplay = 0;
unsigned long currentMillisDisplay = 0;
// Изначально дисплей включен
int sigDisplay = HIGH;

void setup() {
  Serial.begin(57600);
  
  KB.begin(KB1x4, 3000);
  watch.begin();
  Serial.println(watch.gettime("d-m-Y, H:i:s, D"));
  // watch.settime(i[0],i[1],i[2],i[3],i[4],i[5]);
  // initializeEEPROM();
  
  long actualEEPROM = eeprom_crc();           // Получаем актуальное состояние EEPROM при каждом запуске

  while (actualEEPROM == freeEEPROM) {        // Пока отпечатки совпадают, память пуста и её нужно инициализировать
    Serial.println("EEPROM не инициализирована");
    initializeEEPROM();
    actualEEPROM = eeprom_crc();              // Перерасчитываем CRC
  }

  // Программа не продолжит выполнение, пока EEPROM не будет инициализирована
  Serial.println("EEPROM инициализирована успешно");
  
  // Получаем актуальные значения onTime и offTime из EEPROM
  EEPROM.get(0, onTime_h);
  EEPROM.get(1, onTime_m);
  EEPROM.get(2, onTime_s);
  Serial.println((String) "onTime " + onTime_h + ":" + onTime_m + ":" + onTime_s);
  EEPROM.get(3, offTime_h);
  EEPROM.get(4, offTime_m);
  EEPROM.get(5, offTime_s);
  Serial.println((String) "offTime " + offTime_h + ":" + offTime_m + ":" + offTime_s);
  
  pinMode(outputPin, OUTPUT);
  digitalWrite(outputPin, sig); // Поливаем при включении

  // initialize the LED pin as an output:
  pinMode(ledPin, OUTPUT);
  // initialize the pushbutton pin as an input:
  pinMode(btnStartPin, INPUT);
  
  OzOled.init();  //initialze OLED display
  OzOled.printString("On (h:m:s):", 0, 0);
  OzOled.printString("Off (h:m:s):", 0, 5);
  int *len = lenBeforeAndAfter();
  displayNewValues(len, len);
}

void initializeEEPROM() {                     // Процедура заполнения EEPROM начальными значениями
  Serial.println("Инициализируем EEPROM");
  EEPROM.put(0, onTime_h);
  EEPROM.put(1, onTime_m);
  EEPROM.put(2, onTime_s);
  EEPROM.put(3, offTime_h);
  EEPROM.put(4, offTime_m);
  EEPROM.put(5, offTime_s);
}

unsigned long eeprom_crc(void) {              // Функция вычисления CRC EEPROM
  unsigned long crc = ~0L;

  for (int index = 0 ; index < EEPROM.length()  ; ++index) {
    crc = crc_table[(crc ^ EEPROM[index]) & 0x0f] ^ (crc >> 4);
    crc = crc_table[(crc ^ (EEPROM[index] >> 4)) & 0x0f] ^ (crc >> 4);
    crc = ~crc;
  }
  return crc;
}

void turnOff() {
  sig = LOW;  // Turn it off
  digitalWrite(outputPin, sig);
  if(digitalRead(outputPin) == sig) {
    Serial.println(sig);
  }
  ADCSRA &= ~(1 << ADEN); // перед сном отключим АЦП
  ACSR |= (1 << ACD); // и компаратор
  set_sleep_mode(SLEEP_MODE_IDLE);
  sleep_mode();
}

void turnOn() {
  sig = HIGH;  // turn it on
  digitalWrite(outputPin, sig);
  if(digitalRead(outputPin) == sig) {
    Serial.println(sig);
  }
}

void displayNewValues(int *lenBefore, int *lenAfter) {
  char buf_h [4], buf_m [4], buf_s [4];
  onTimeString = itoa(onTime_h, buf_h, 10) + String(":") + itoa(onTime_m, buf_m, 10) + String(":") + itoa(onTime_s, buf_s, 10);
  // Если новое значение меньше предыдущего по длине, добавляем пробел в конец
  int diff = lenAfter[0] - lenBefore[0];
  if (diff == -1) {
    onTimeString += " ";
  } else if (diff == -2) {
    onTimeString += "  ";
  } else if (diff == -3) {
    onTimeString += "   ";
  }
  const char *onTimeChar = onTimeString.c_str();

  char buff_h [4], buff_m [4], buff_s [4];
  offTimeString = itoa(offTime_h, buff_h, 10) + String(":") + itoa(offTime_m, buff_m, 10) + String(":") + itoa(offTime_s, buff_s, 10);
  diff = lenAfter[1] - lenBefore[1];
  if (diff == -1) {
    offTimeString += " ";
  } else if (diff == -2) {
    offTimeString += "  ";
  } else if (diff == -3) {
    offTimeString += "   ";
  }
  const char *offTimeChar = offTimeString.c_str();
  OzOled.printString16(onTimeChar, 0, 2);
  OzOled.printString16(offTimeChar, 0, 6);
}

bool displayChecking() {
  if(sigDisplay == LOW) {
    previousMillisDisplay = currentMillisDisplay;
    sigDisplay = HIGH;
    OzOled.setPowerOn();
    return false;
  } else {
    return true;
  }
}

// Определяет длину строки до и после изменения значения
int *lenBeforeAndAfter() {
  char onBuf_h[5], onBuf_m[5], onBuf_s[5];
  char offBuf_h[5], offBuf_m[5], offBuf_s[5];
  unsigned int onTime_h_Len = sprintf(onBuf_h,"%d", onTime_h);
  unsigned int onTime_m_Len = sprintf(onBuf_m,"%d", onTime_m);
  unsigned int onTime_s_Len = sprintf(onBuf_s,"%d", onTime_s);
  unsigned int onTimeLen = onTime_h_Len + onTime_m_Len + onTime_s_Len + 2; /* +2 - это два двоеточия */
  unsigned int offTime_h_Len = sprintf(offBuf_h,"%d", offTime_h);
  unsigned int offTime_m_Len = sprintf(offBuf_m,"%d", offTime_m);
  unsigned int offTime_s_Len = sprintf(offBuf_s,"%d", offTime_s);
  unsigned int offTimeLen = offTime_h_Len + offTime_m_Len + offTime_s_Len + 2; /* +2 - это два двоеточия */
  int* arr = new int[2];
  arr[0]= onTimeLen;
  arr[1]= offTimeLen;
  return arr;
}

void loop() {
  // check to see if it's time to change the state of the outputPin
  currentMillis = millis();
  
  // Таймер дисплея
  currentMillisDisplay = millis();
  
  if(currentMillis%1000==0){
    watch.gettime();
    // Задаем условие для включения полива
    if(watch.Hours==onTime_h && watch.minutes==onTime_m && watch.seconds==onTime_s){
      turnOn();
    }
    // Задаем условие для выключения полива
    if(watch.Hours==offTime_h && watch.minutes==offTime_m && watch.seconds==offTime_s){
      turnOff();
    }
  }

  // Если флаг активности дисплея поднят и текущее значение таймера превышает 5 сек, отключаем дисплей
  if((sigDisplay == HIGH) && (currentMillisDisplay - previousMillisDisplay >= onTimeDisplay)) {
    sigDisplay = LOW;
    previousMillisDisplay = currentMillisDisplay;
    OzOled.setPowerOff();
  }

  // read the state of the pushbutton value:
  btnStartState = digitalRead(btnStartPin);
  if (btnStartState == HIGH) {
    Serial.println("Button down");
    digitalWrite(ledPin, HIGH);
    if (sig == LOW) {
      // Запускаем полив форсированно
      turnOn();
      delay(500);
    } else if (sig == HIGH) {
      Serial.println("SIG == HIGH");
      turnOff();
      delay(500);
    }
    digitalWrite(ledPin, LOW);
  }

  // Мембранные кнопки
  KB.read();

  if (KB.justPressed() && displayChecking()) {
    switch (KB.getNum) {
      case 1:
        // Устанавливаем время начала и окончания полива (часы)
        if(sigDisplay == HIGH) {
          int *lenBefore = lenBeforeAndAfter();
          onTime_h += 1;
          offTime_h += 1;
          if (onTime_h == 24 && offTime_h == 24) {
            onTime_h = 0;
            offTime_h = 0;
          }
          int *lenAfter = lenBeforeAndAfter();
          EEPROM.put(0, onTime_h);
          EEPROM.put(3, offTime_h);
          displayNewValues(lenBefore, lenAfter);
          delete[] lenBefore;
          delete[] lenAfter;
        }
      break;
      case 2:
        // Устанавливаем время начала полива (минуты)
        if(sigDisplay == HIGH) {
          int *lenBefore = lenBeforeAndAfter();
          onTime_m += 1;
          if (onTime_m == 60) {
            onTime_m = 0;
          }
          int *lenAfter = lenBeforeAndAfter();
          EEPROM.put(1, onTime_m);
          displayNewValues(lenBefore, lenAfter);
          delete[] lenBefore;
          delete[] lenAfter;
        }
      break;
      case 3:
        // Устанавливаем время окончания полива (минуты)
        if(sigDisplay == HIGH) {
          int *lenBefore = lenBeforeAndAfter();
          offTime_m += 1;
          if (offTime_m == 60) {
            offTime_m = 0;
          }
          int *lenAfter = lenBeforeAndAfter();
          EEPROM.put(4, offTime_m);
          displayNewValues(lenBefore, lenAfter);
          delete[] lenBefore;
          delete[] lenAfter;
        }
      break;
      case 4:
        // Устанавливаем время окончания полива (секунды)
        if(sigDisplay == HIGH) {
          int *lenBefore = lenBeforeAndAfter();
          offTime_s += 1;
          if (offTime_s == 60) {
            offTime_s = 0;
          }
          int *lenAfter = lenBeforeAndAfter();
          EEPROM.put(5, offTime_s);
          displayNewValues(lenBefore, lenAfter);
          delete[] lenBefore;
          delete[] lenAfter;
        }
      break;
    }
  }
  // При зажатой кнопке 1 - сброс значений
  if (KB.isHold() && displayChecking()) {
    if (KB.getNum == 1) {
      int *lenBefore = lenBeforeAndAfter();
      onTime_h = 0;
      onTime_m = 0;
      offTime_h = 0;
      offTime_m = 0;
      offTime_s = 0;
      int *lenAfter = lenBeforeAndAfter();
      EEPROM.put(0, onTime_h);
      EEPROM.put(1, onTime_m);
      EEPROM.put(3, offTime_h);
      EEPROM.put(4, offTime_m);
      EEPROM.put(5, offTime_s);
      displayNewValues(lenBefore, lenAfter);
      delete[] lenBefore;
      delete[] lenAfter;
    }
  }
}
