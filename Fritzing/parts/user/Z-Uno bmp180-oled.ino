#include "Wire.h"
#include "ZUNO_BMP180.h"
#include "ZUNO_OLED_I2C.h"

ZUNO_BMP180 bmp;
OLED oled;

void setup() {
  bmp.begin();
  oled.begin();
  oled.clrscr();
}
  
void loop() {
  delay(1000);
  oled.gotoXY(0,0);
  oled.print("Millis:");
  oled.print(millis());
    
  oled.gotoXY(0,1);
  oled.print("Temperature ");
  oled.print(bmp.readTemperature());
  oled.println(" *C");
    
  oled.print("Pressure ");
  oled.print(bmp.readPressure()/133.32);
  oled.println(" mm");

  delay(2000);
}
