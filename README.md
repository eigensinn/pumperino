# pumperino

Irrigation system arduino project. Arduino Nano v3, Mini360 DC-DC, IRF520N Mosfet module, SSD1306 OLED, RTC_DS1302, 1x4 matrix keyboard, water pump.

License: [GPLv3](https://bitbucket.org/eigensinn/pumperino/src/master/LICENSE.md).